package net.ib.security;

import org.apache.commons.codec.net.URLCodec;

public class IBTest {

    public static void main(String[] args) {
		String id = "ib";			// 회원사 ID
		String svc_code = "ib1";	// 호출 URL 에 발급되는 코드값	String req_code = "abc123"; // 요청코드 (임의생성) - 결과 복호화시 사용됨
		String req_code = "abc123"; // 요청코드 (임의생성) - 결과 복호화시 사용됨
		String requestParam = "id="+id;
		requestParam = requestParam + "&svc_code="+svc_code;
		requestParam = requestParam + "&req_code=" + req_code;	// 요청번호-암호화된 결과의 복호화에 사용되는 값으로 요청시마다 발급되어야 함
		requestParam = requestParam + "&name=박재선&sex=1&bank_code_std=097&bank_holder_number=111222333444&birth_date=770308";	// Option ( 기본자료가 존재할 경우)
		requestParam = requestParam + "&ex1=a&ex2=b";		// 기타 옵션 PARAM
		
		System.out.println(requestParam);
		
		String enc = "";
		try {
			IBCipher cipher = new IBCipher();
			URLCodec codec = new URLCodec();
			enc = cipher.pubEncode(requestParam);
			System.out.println(enc);
			String req_info = codec.encode(enc);
			System.out.println(req_info);
			System.out.println("\n\n");

			String ret_info = req_info;
			String dec = codec.decode(ret_info);
			System.out.println(dec);
			IBCipher cipher1 = new IBCipher(req_code);
			String decData = cipher1.aesDecode(dec);
			System.out.println(decData);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("Exception occurred");
		}
		
        System.out.println("OK");
    }
}
